import Readt from 'react';

import Introduction from '../secitons/Introduction';
import TechnologyList from '../secitons/TechnologyList';


const TechnologyPage = ({data}) => {
const {introduction,technologyList} = data;
	return (
		<div className="technology-page"
			<Introduction className="technology-page__introduction" data={introduction}/>
			<TechnologyList className="technology-page__technology-list" data={technologyList}/>
		</React.Fragment>
	}
}

export default TechnologyPage;
