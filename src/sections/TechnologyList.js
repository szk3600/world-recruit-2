import Readt from 'react';

import Card from '../components/Card.js';

const TechnologyList = ( {data}) => {
	return (
		<React.Fragment>
			{data.techonologies.map ((techonology,index) => {
				return (
						<Card data={techonology} />
				);
			}) }
		</React.Fragment>
};

export default TechnologyList;
