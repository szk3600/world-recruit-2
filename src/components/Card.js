import Readt from 'react';

const Card = ({data}) => {
	return <div className="card">
		<h1 className="card__ttile">{data.title}</h1>
		<p  className="card__description">{data.description}</p>
		<img className="card__image" {...data.img} />
		</div>
}

export default Card;
